import { Keyboard, KeyboardAvoidingView, Platform, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Task from './components/Task';
import { useState } from 'react';
import EditTaskModal from './components/EditTaskModal';

export default function App() {
  const [task, setTask] = useState(null);
  const [taskItems, setTaskItems] = useState([]);
  const [pressedTask, setPressedTask] = useState(null);

  const [isEditModalVisible, setEditModalVisible] = useState(false);
  const [editableTaskIndex, setEditableTaskIndex] = useState(null);
  const [editableTaskText, setEditableTaskText] = useState("");

  const handleAddTask = () => {
    if (!task) return;

    Keyboard.dismiss();
    const newTask = { text: task, completed: false }
    setTaskItems([...taskItems, newTask]);
    setTask(null);
  }

  const handleCompleteTask = (index) => {
    let itemsCopy = [...taskItems];
    itemsCopy[index].completed = !itemsCopy[index].completed;
    setTaskItems(itemsCopy);
  }

  const deleteTask = (index) => {
    let itemsCopy = [...taskItems];
    itemsCopy.splice(index, 1);
    setTaskItems(itemsCopy);
  }

  // Funções para o modal de edição

  const openEditModal = (index) => {
    setEditableTaskIndex(index);
    setEditableTaskText(taskItems[index].text);
    setEditModalVisible(true);
  };

  const handleEditTask = () => {
    let itemsCopy = [...taskItems];
    if (editableTaskIndex !== null) {
      itemsCopy[editableTaskIndex].text = editableTaskText;
      setTaskItems(itemsCopy);
    }
    closeEditModal();
  };

  const closeEditModal = () => {
    setEditModalVisible(false);
    setEditableTaskIndex(null);
  };

  return (
    <View style={styles.container}>
      <EditTaskModal
        isVisible={isEditModalVisible}
        taskText={editableTaskText}
        onChangeText={setEditableTaskText}
        onSave={handleEditTask}
        onCancel={closeEditModal}
      />
      <View style={styles.tasksWrapper}>
        <Text style={styles.sectionTitle}>Tarefas</Text>

        <View style={styles.items}>
          {
            taskItems.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  onPressIn={() => { setPressedTask(index) }}
                  onPressOut={() => { setPressedTask(null) }}
                  onLongPress={() => deleteTask(index)}
                  style={{
                    backgroundColor: pressedTask === index ?? '#f0f0f0',
                  }}
                >
                  <Task text={item.text} completed={item.completed} onCompletePress={() => handleCompleteTask(index)} onTextPress={() => openEditModal(index)} />
                </TouchableOpacity>
              )
            })
          }
        </View>
      </View>

      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.writeTaskWrapper}
      >
        <TextInput
          style={styles.input}
          placeholder={'Adicionar tarefa'}
          placeholderTextColor={'#C0C0C0'}
          onChangeText={(text) => setTask(text)}
          value={task}
        />
        <TouchableOpacity onPress={() => handleAddTask()}>
          <View style={styles.addWrapper}>
            <Text style={styles.addText}>+</Text>
          </View>
        </TouchableOpacity>
      </KeyboardAvoidingView>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E8EAED',
  },
  tasksWrapper: {
    paddingTop: 80,
    paddingHorizontal: 20,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  items: {
    marginTop: 30,
  },
  writeTaskWrapper: {
    position: 'absolute',
    bottom: 60,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  input: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    width: 250,
    backgroundColor: '#FFF',
    borderRadius: 60,
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  addWrapper: {
    width: 60,
    height: 60,
    backgroundColor: '#FFF',
    borderRadius: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  addText: {},
});
