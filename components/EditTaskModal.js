import React from 'react';
import { Modal, View, TextInput, Button, StyleSheet } from 'react-native';

const EditTaskModal = ({ isVisible, onChangeText, onSave, onCancel, taskText }) => {
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={isVisible}
            onRequestClose={onCancel}
        >
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <TextInput
                        style={styles.input}
                        onChangeText={onChangeText}
                        value={taskText}
                        placeholder="Editar Tarefa"
                    />
                    <View style={styles.buttonContainer}>
                        <Button title="Salvar" onPress={onSave} />
                        <Button title="Cancelar" onPress={onCancel} />
                    </View>
                </View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    input: {
        width: 300,
        height: 40,
        marginBottom: 15,
        borderWidth: 1,
        padding: 10,
        borderRadius: 10,
        borderColor: '#ddd'
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    // ver se é necessario
    modalButton: {
        width: 100,
        borderRadius: 10,
    }
});

export default EditTaskModal;
