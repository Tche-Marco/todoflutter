import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

const Task = (props) => {
    return (
        <View style={styles.item}>
            <TouchableOpacity
                style={styles.itemText}
                onPress={props.onTextPress}
            >
                <Text>{props.text}</Text>
            </TouchableOpacity>
            {
                props.completed ?
                    <TouchableOpacity
                        style={styles.check}
                        onPress={props.onCompletePress}
                    ></TouchableOpacity>
                    :
                    <TouchableOpacity
                        style={styles.circular}
                        onPress={props.onCompletePress}
                    ></TouchableOpacity>
            }
        </View >
    );
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: "#FFF",
        padding: 15,
        borderRadius: 10,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginBottom: 20,
    },
    itemText: {
        maxWidth: "80%",
        paddingLeft: 10,
    },
    circular: {
        width: 12,
        height: 12,
        borderColor: "#55BCF6",
        borderWidth: 2,
        borderRadius: 5,
    },
    check: {
        width: 12,
        height: 12,
        backgroundColor: "#55BCF6",
        borderRadius: 5,
    },
});

export default Task;